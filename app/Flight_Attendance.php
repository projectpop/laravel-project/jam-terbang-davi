<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight_Attendance extends Model
{
    protected $table = 'flight_attendance';
    protected $primaryKey = 'id';
}
