<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://localhost:3000/api/v1/berita', // Link API
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET', // Method request
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFkbWluIiwiaWF0IjoxNjE0MDM5OTU1fQ.cj34mVSpv1EBXoWYrrr2UsAQizE0f0JmUssa--ua8ao'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

//        dd($response);
        $berita = json_decode($response);

        return view('flight/db-berita')
            ->with('berita',$berita);
    }

    public function add()
    {
        return view('flight/db-berita-add');
    }

    public function save(Request $req)
    {
        // Param yang mau dikirim ke API
        $dataSend = [
            "judul" => $req['judul'],
            "konten" => $req['konten'],
        ];

        $params = json_encode($dataSend);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://localhost:3000/api/v1/berita', // Link API
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST', // Method request
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFkbWluIiwiaWF0IjoxNjE0MDM5OTU1fQ.cj34mVSpv1EBXoWYrrr2UsAQizE0f0JmUssa--ua8ao'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

//        dd($response);

        return view('flight/db-berita');
    }
}
