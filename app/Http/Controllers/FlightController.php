<?php

namespace App\Http\Controllers;

use App\Flight_Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FlightController extends Controller
{
    public function index()
    {
        $flights = DB::select( DB::raw("SELECT A.id as flight_id, A.flight_date, A.registration, A.captain_id, A.flight_hours, A.user_id,
                   A.route_from, A.route_to,
                   B1.ICAO as ICAO_from, B1.IATA as IATA_from, B1.city as city_from, B1.province as province_from,
                   B2.IATA as IATA_to, B2.IATA as IATA_to, B2.city as city_to, B2.province as province_to
            FROM flight_attendance as A
            LEFT JOIN airports as B1 on B1.id = A.route_from
            LEFT JOIN airports as B2 on B2.id = A.route_to
            where A.user_id = :user_id;"),
        array('user_id' => 1,
        ));

        return view('flight/index')
            ->with('flights',$flights);
    }

    public function add()
    {
        return view('flight/add');
    }
}
