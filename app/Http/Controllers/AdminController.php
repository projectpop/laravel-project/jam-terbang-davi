<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('frames/main');
    }


    public function tables()
    {
        return view('tables');
    }

    public function sidebar()
    {
        return view('sidebar');
    }

    public function tes()
    {
        return view('frames2/main');
    }
}
