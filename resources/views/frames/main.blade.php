<!DOCTYPE html>

<html lang="{{ config('app.locale') }}">
<head>
  <!-- Head -->
  @include('frames.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Main Sidebar Container -->
  @include('frames.sidebar')

  <!-- Projects -->
  @yield('project')

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@include('frames.footer-script')

</body>
</html>
