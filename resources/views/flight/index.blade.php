@extends('flight.layouts.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Flight Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Simple Tables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="btn btn-primary btn-sm float-left" href="{{ URL::route('flightAdd') }}">
                  <i class="fas fa-plus"></i> Add Route
                </a>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>REG</th>
                    <th>Captain</th>
                    <th>Flight Hours</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($flights as $idx => $flights)
                    <tr>
                      <td>{{$flights->flight_date}}</td>
                      <td>{{$flights->city_from}} ({{$flights->IATA_from}}) - {{$flights->city_to}} ({{$flights->IATA_to}})</td>
                      <td>{{$flights->registration}}</td>
                      <td>{{$flights->captain_id}}</td>
                      <td>{{$flights->flight_hours}}</td>
                      <td><!-- align="center"-->
                        <a class="btn btn-sm btn-warning" href=""><div class="fas fa-edit"></div></a>
                        <a class="btn btn-sm btn-danger" href=""><div class="fas fa-trash-alt"></div></a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
@endsection