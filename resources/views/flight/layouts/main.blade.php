<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  @include('flight.layouts.head')
</head>
<body class="hold-transition sidebar-mini">

  <div class="wrapper">

    <!-- Sidebar & Navbar -->
    @include('flight.layouts.header')

      <!-- Content -->
      @yield('content')

    <!-- Footer -->
    @include('flight.layouts.footer')

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->
  @include('flight.layouts.footer-script')

</body>
</html>
