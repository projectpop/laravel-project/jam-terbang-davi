<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="float-right d-none d-sm-inline">
    <i class="nav-icon fas fa-bed"></i> Kuli-Kode
  </div>
  <!-- Default to the left -->
  <strong>Copyright &copy; 2021 <a href="https://www.linkedin.com/in/teddy-syaifuddin/">Teddy Syaifuddin</a>.</strong> All rights reserved.
</footer>