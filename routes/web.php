<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

/**
 * Dashboard Master
 */
Route::get('/dashboard', 'AdminController@index')->name('home');


/**
 * Flight Attendance Apps
 */
Route::get('/flight-attendance', 'FlightController@index')->name('flight');
Route::get('/flight-add', 'FlightController@add')->name('flightAdd');

Route::get('/tables', 'AdminController@tables');
Route::get('/sidebar', 'AdminController@sidebar');


Route::get('/tes', 'AdminController@tes');


/**
 * Berita
 */
Route::get('/dashboard-berita', 'DashboardController@index')->name('berita');
Route::get('/dashboard-berita-add', 'DashboardController@add')->name('beritaAdd');
Route::post('/dashboard-berita-save', 'DashboardController@save')->name('beritaSave');